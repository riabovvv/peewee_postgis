# Peewee-PostGIS

Adds `PostGIS` data types support to the [Peewee](http://docs.peewee-orm.com/en/latest/).

In order for this package to function it is needed to install either `GDAL` or `pygdal` 
package of the version corresponding to version of GDAL (2.0 or higher) installed in your system. 
