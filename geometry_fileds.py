import json
import string

from functools import singledispatch, update_wrapper

# Third party
import shapely.wkb as shpl_wkb
from typing import Union, Any
from osgeo import ogr

# Database
from osgeo.ogr import Geometry
from playhouse import postgres_ext
from peewee import OP, Expression, Field, fn
from postgis import LineString, Point


def methoddispatch(func):
    """Декоратор для перегрузки методов классов"""
    dispatcher = singledispatch(func)

    def wrapper(*args, **kwargs):
        return dispatcher.dispatch(args[1].__class__)(*args, **kwargs)
    wrapper.register = dispatcher.register
    update_wrapper(wrapper, func)
    return wrapper


ogr.UseExceptions()

OP.update(
    BBOX2D='&&',
    BBOXCONTAINS='~',
    BBOXCONTAINED='@',
)
postgres_ext.PostgresqlExtDatabase.register_ops({
    OP.BBOX2D: OP.BBOX2D,
    OP.BBOXCONTAINS: OP.BBOXCONTAINS,
    OP.BBOXCONTAINED: OP.BBOXCONTAINED,
})


class GeometryField(postgres_ext.IndexedFieldMixin, Field):
    """"""
    index_type = 'GiST'
    _geometry_types = dict(
        geometry='GEOMETRY',
        point='POINT',
        multipoint='MULTIPOINT',
        linestring='LINESTRING',
        multilinestring='MULTILINESTRING',
        polygon='POLYGON',
        multipolygon='MULTIPOLYGON',
        circularstring='CIRCULARSTRING',
        compoundcurve='COMPOUNDCURVE',
        curvepolygon='CURVEPOLYGON',
        surface='SURFACE',
        multisurface='MULTISURFACE',
        polyhedralsurface='POLYHEDRALSURFACE',
        curve='CURVE',
        multicurve='MULTICURVE',
        geometrycollection='GEOMETRYCOLLECTION',
    )
    _dimensions = ('Z', 'M', 'ZM')
    _default_srid = 4326
    _ogr_geom = None
    is_geography = False

    @property
    def _default_geometry_type(self):
        return self._geometry_types['geometry']

    def __init__(self, geography: bool=False, geom_type: str=None,
                 geom_srid: int=None, dimension: str=None, **kwargs):
        geom_type = geom_type.lower()
        geom_type = self._geometry_types[geom_type] if geom_type is not \
            None else self._geometry_types[self._default_geometry_type]
        dimension = dimension.upper() if dimension is not None else ''
        if dimension not in ['', *self._dimensions]:
            raise ValueError(f'Invalid dimension: "{dimension}"')
        self.geom_type = f'{geom_type}{dimension}'
        self.srid = geom_srid if geom_srid is not None else self._default_srid
        self.base_spatial_type = {
            True: 'geography',
            False: 'geometry'
        }.get(geography)
        if self.base_spatial_type is None:
            raise ValueError(f'Invalid `geography` value: "{geography}"')
        self.is_geography = geography
        self.db_field = (f'{self.base_spatial_type}({self.geom_type}, '
                         f'{self.srid})')  # 'geometry'
        super().__init__(**kwargs)

    @property
    def ogr_geom(self):
        return self._ogr_geom

    @property
    def ogr_crs(self):
        ogr_srs = ogr.osr.SpatialReference()
        ogr_srs.ImportFromEPSG(self.srid)
        return ogr_srs

    def validate_geom_type(self, geom):
        geom_type = geom.GetGeometryName()
        if self.geom_type != geom_type:
            raise ValueError(f'Invalid geometry type: "{geom_type}"')

    def _fix_geom_crs(self, geom: Geometry) -> None:
        geom_crs = geom.GetSpatialReference()
        if geom_crs is None:
            geom.AssignSpatialReference(self.ogr_crs)
        else:
            # TODO: нужна ли тут проверка на то, что геометрия уже в нужной
            # проекции?
            geom.TransformTo(self.ogr_crs)

    def db_value(self, value: Any) -> Union[str, None]:
        geom = self.coerce(value)
        if geom is None:
            return geom
        wkt = geom.ExportToIsoWkt()
        ewkt = f'SRID={self.srid};{wkt}'
        return ewkt

    def python_value(self, value) -> Union[dict, None]:
        """Возвращает """
        if value is None:
            return value
        geojson_string = self.coerce(value).ExportToJson()
        gejson = json.loads(geojson_string)
        return gejson

    @methoddispatch
    def coerce(self, value: Any) -> Union[Geometry, None]:
        raise NotImplementedError(f'Unsupported data type "{type(value)}"')

    @coerce.register(Geometry)
    def _(self, value: Geometry):
        self._fix_geom_crs(value)
        self._ogr_geom = value
        return self.ogr_geom

    @coerce.register(type(None))
    def _(self, value: None):
        self._ogr_geom = value
        return self.ogr_geom

    @coerce.register(str)
    def _(self, value: str):
        """Expects WKT, geojson or hexadecimal-string (WKB)"""
        if '{' in value:
            # Seems it is a geojson
            value = json.loads(value)
            return self.coerce(value)
        try:
            geom = ogr.CreateGeometryFromWkt(value)
        except RuntimeError:
            if all(char in string.hexdigits for char in value):
                # We have a hexadecimal string which means it is actually a WKB
                # TODO: избавиться от конвертации WKB->WKT
                wkt = shpl_wkb.loads(value, hex=True).wkt
                geom = ogr.CreateGeometryFromWkt(wkt)
            else:
                raise ValueError('Invalid geometry data')
        self.validate_geom_type(geom)
        self._fix_geom_crs(geom)
        self._ogr_geom = geom
        return self.ogr_geom

    @coerce.register(dict)
    def _(self, value: dict):
        value = json.dumps(value)
        geom = ogr.CreateGeometryFromJson(value)
        self.validate_geom_type(geom)
        self._fix_geom_crs(geom)
        self._ogr_geom = geom
        return self.ogr_geom

    @coerce.register(bytes)
    def _(self, value: bytes):
        geom = ogr.CreateGeometryFromWkb(value)
        self.validate_geom_type(geom)
        self._fix_geom_crs(geom)
        self._ogr_geom = geom
        return self.ogr_geom

    def contained_by(self, geom):
        return Expression(self, OP.BBOXCONTAINED, geom)

    def contains(self, geom):
        return Expression(self, OP.BBOXCONTAINS, geom)

    def in_bbox(self, south, north, east, west):
        return self.contained_by(
            fn.ST_MakeBox2D(Point(west, south, srid=self.srid),
                                   Point(east, north, srid=self.srid)),
            )


class PointField(GeometryField):

    def __init__(self, *args, **kwargs):
        super().__init__(geom_type='point', *args, **kwargs)


class LineStringField(GeometryField):

    def __init__(self, *args, **kwargs):
        super().__init__(geom_type='linestring', *args, **kwargs)
